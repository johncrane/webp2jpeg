# webp2jpeg

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/johncrane/webp2jpeg.git
git branch -M main
git push -uf origin main
```

## Description
Converts all webp image files in the current working directory to jpgs no larger that 2M.

## Usage
```
python3 web2jpg.py
```

## To do
* Set the max size of the jpg to be a user parameter

## License
Free to use without warranty. No attribution is required. 

## Project status
Not actively supported
