import os
import sys
from PIL import Image

# Global variables
gMaxSize = 0 
gCompression = 0

# print usage example and help on options and commands
def usage():
	options = '''John's little image manipulation tool

Usage
	webp2jpg [options] command

Options:
	-h       -- show usage
	-maxSize -- set maximum file size, in kilobytes, defaults to 2048
	-quality -- set compression level

Commands:
	webp     -- convert jpgs to webp, no larger than maxSize 
	jpg      -- if maxSize is used compress existing jpg to a new jpg file no larger than maxSize
	         -- if -quality is used compress existing jpg to a new file using quality setting
'''
	print(options)


def convertWebpsToJpgs():
	global gMaxSize

	print("Converting files from WebP to Jpg\n")			
	cwd = os.getcwd()
	formats = ['.webp']
	newSize = 0
	quality = 95
	newFile = ''
	
	# loop through files in the current directory
	for file in os.listdir(cwd):
		if os.path.splitext(file)[1].lower() in formats:
			print(file, end = " - ")
			isSmallEnough = False
			quality = 95
			newSize = 0
			# compress with decreasing quality until < gMaxSize
			while not isSmallEnough:
				newFile = convert2jpg(file, quality)
				newSize = os.path.getsize(newFile)
				if newSize <= gMaxSize or quality < 25:
					isSmallEnough = True
				else:
					quality -= 5
					os.remove(newFile)
					
			else:
				isSmallEnough = True
			print(f"{quality=}%, file size = {newSize}" )					


def compressJpegs():
	global gMaxSize
	global gCompression

	print("Converting files from WebP to Jpg\n")			
	cwd = os.getcwd()
	formats = ['.jpeg', '.jpg']
	newSize = 0
	quality = 95
	newFile = ''
	
	# loop through files in the current directory
	for file in os.listdir(cwd):
		if os.path.splitext(file)[1].lower() in formats:
			print(file, end = " - ")

			if gCompression > 0:
				newFile = convert2jpg(file, gCompression)
				newSize = os.path.getsize(newFile)
				print(f"compression = {gCompression}, file size = {newSize}" )

			elif gMaxSize > 0:
				# compress with decreasing quality until < gMaxSize
				isSmallEnough = False
				quality = 95
				newSize = 0
				while not isSmallEnough:
					newFile = convert2jpg(file, quality)
					newSize = os.path.getsize(newFile)
					if newSize <= gMaxSize or quality < 10:
						isSmallEnough = True
					else:
						quality -= 5
						os.remove(newFile)		

				print(f"{quality=}%, file size = {newSize}" )



# load file, save as jpg, return filename of new jpg
def convert2jpg(file, quality):
	
	filepath = os.path.join(os.getcwd(), file)
	picture = Image.open(filepath)
	newFileName = file + str(quality) + ".jpg"
	
	picture.save(newFileName,
				"JPEG",
				optimize = True,
				subsampling = 0,
				quality = quality)
	
	return newFileName

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# convert all webp images in the cwd to jpgs, no larger that 2M
def main():
	global gMaxSize
	global gCompression

	args = sys.argv[1:]
	print(f"\n{sys.argv[0]} \n")

	for index, arg in enumerate(args):

		if "-h" == arg:
			usage()
	
		if "-maxsize" == arg:
			if len(args) <= index + 1:
				print("Bad maxSize argument\n")
			else:
				k = int(args[index+1])
				if k < 5:
					print("Bad maxSize argument\n")
				else:
					gMaxSize = k*1024
			print(f"maxSize = {gMaxSize//1024}k\n")

		if "-compression" == arg:
			if len(args) <= index + 1:
				print("Bad compression argument\n")
			else:
				k = int(args[index+1])
				if k < 5 or k > 100:
					print("Bad compression argument\n")
				else:
					gCompression = k
			print(f"compression = {gCompression}% \n")

		if "webp" == arg:		
			convertWebpsToJpgs()
			print("Done")
			return

		if "jpg" == arg:	
			if (gMaxSize + gCompression == 0):
				usage()
				print(bcolors.FAIL + "Need to specify maxsize or compression to use the jpg command\n " + bcolors.ENDC)	
				return
			
			compressJpegs()
			print("Done")
			return

	usage()

if __name__ == "__main__":
	main()

